import Vue from 'vue'

export function watchDepthUpdates (symbol, cb) {
  Vue.http.ws.init(`${symbol.toLowerCase()}@depth`)
  Vue.http.ws.on('message', e => {
    cb(e.data)
  })
}

export async function getDepthSnapshot (symbol) {
  return Vue.http.rest.get(`depth?symbol=${symbol}&limit=500`)
}
