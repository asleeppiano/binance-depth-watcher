import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Api from './plugins/api/index'
import EventBus from './plugins/event-bus/index'
import ReactiveProvide from 'vue-reactive-provide'

Vue.use(ReactiveProvide)
Vue.use(Api)
Vue.use(EventBus)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
