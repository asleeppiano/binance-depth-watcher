const WS_BASE_ENDPOINT = 'wss://stream.binance.com:9443/ws'
const REST_BASE_ENDPOINT = 'https://api.binance.com'

class BinanceDepthCache {
  constructor (symbol) {
    this._symbol = symbol
    this._bids = {}
    this._asks = {}
    this._updateTime = null
  }

  addBid (bid) {
    if (bid[1] === '0.00000000') {
      delete this._bids[bid[0]]
      return
    }
    this._bids[bid[0]] = Number(bid[1])
  }

  addAsk (ask) {
    if (ask[1] === '0.00000000') {
      delete this._asks[ask[0]]
      return
    }
    this._asks[ask[0]] = Number(ask[1])
  }

  get bids () {
    return this.sortDepth(this._bids, { reverse: true })
  }

  get asks () {
    return this.sortDepth(this._asks, { reverse: false })
  }

  sortDepth (data, { reverse = false }) {
    const list = Object.entries(data).map(([k, v]) => [Number(k), v])
    let sortFunc = (a, b) => a[0] - b[0]
    if (reverse) {
      sortFunc = (a, b) => b[0] - a[0]
    }
    list.sort(sortFunc)
    return list
  }
}

export class BinanceDepthApi {
  start (symbol, cb) {
    this._lastUpdateId = null
    this._depthMessageBuffer = []
    this._symbol = symbol
    this._updateTime = null
    this._refreshInterval = 3600 * 1000
    this._depthCache = new BinanceDepthCache(symbol)
    this.cb = null

    this.symbol = symbol
    this.cb = cb
    this.startSocket()
    this.initCache()
  }

  async fetchDepthSnapshot (symbol = 'BTCUSDT', limit = 500) {
    const res = await fetch(
      `${REST_BASE_ENDPOINT}/api/v3/depth?symbol=${symbol}&limit=${limit}`
    )
    const data = res.json()

    if (res.ok) {
      return data
    } else {
      throw new Error('cannot get data')
    }
  }

  async initCache () {
    this.lastUpdateId = null
    this._depthMessageBuffer = []

    this.fetchDepthSnapshot(this.symbol).then(orderBook => {
      orderBook.bids.forEach(bid => {
        this._depthCache.addBid(bid)
      })
      orderBook.asks.forEach(ask => {
        this._depthCache.addAsk(ask)
      })

      if (this._refreshInterval) {
        this._refreshTime = Date.now() + this.refreshInterval
      }

      this._lastUpdateId = orderBook.lastUpdateId

      this._depthMessageBuffer.forEach(msg => {
        this.processDepthMessage(msg)
      })

      this._depthMessageBuffer = []
    })
  }

  async startSocket () {
    this._socket = new WebSocket(`${WS_BASE_ENDPOINT}/${this.symbol.toLowerCase()}@depth`)
    this._socket.addEventListener('message', this._socketHandler.bind(this))
  }

  _socketHandler (e) {
    const msg = JSON.parse(e.data)
    if (!this._lastUpdateId) {
      this._depthMessageBuffer.push(msg)
    } else {
      this.processDepthMessage(msg)
    }
  }

  processDepthMessage (msg) {
    if (msg.u <= this._lastUpdateId) {
      return
    }

    msg.b.forEach(bid => {
      this.depthCache.addBid(bid)
    })
    msg.a.forEach(ask => {
      this.depthCache.addAsk(ask)
    })

    this._updateTime = msg.E

    this.cb(this._depthCache, msg)

    this._lastUpdateId = Number(msg.u)

    if (Date.now() > this._refreshTime) {
      console.log('initCache')
      this.initCache()
    }
  }

  close () {
    if (this._socket) {
      this._socket.removeEventListener('message', this._socketHandler)
      this._socket.close()
      this._depthCache = null
    }
  }

  get socket () {
    return this._socket
  }

  get depthCache () {
    return this._depthCache
  }
}
