import { BinanceDepthApi } from './api'

function Api (Vue) {
  const api = new BinanceDepthApi()

  Object.defineProperty(Vue, 'api', {
    get () {
      return api
    }
  })

  Object.defineProperty(Vue.prototype, '$api', {
    get () {
      return api
    }
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(Api)
}

export default Api
